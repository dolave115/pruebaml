﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CrudML.Models;
using CrudML.Models.ViewModels;

namespace CrudML.Controllers
{
    public class TablaController : Controller
    {
        // GET: Tabla
        public ActionResult Index()
        {
            List<ListTablaViewModel> lst;
            using(productoEntities db=new productoEntities())
            
                {
                 lst= (from d in db.crudcreate
                            select new ListTablaViewModel
                         {
                            Id = d.id,
                            Nombre=d.nombre_cliente,
                            tipo_documento=d.tipo_documento,    
                            direccion=d.direccion,
                            telefono=d.telefono,
                            correo=d.correo,    
                            fecha_creacion=d.fecha_creacion,    

                         }).ToList();
                }
            return View(lst);
        }
        public ActionResult Nuevo() 
        {
            return View();

        }
        [HttpPost]
        public ActionResult Nuevo(TablaViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (productoEntities db = new productoEntities())
                    {
                       db.SPINSERT1(model.Id, model.Nombre, model.tipo_documento, model.direccion, model.telefono, model.correo, model.fecha_creacion);
                       
                        
                        db.SaveChanges();
                          

                    }
                    return Redirect("~/Tabla/");
                }
                return View(model);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }
        public ActionResult Editar(int Id)
        {
            TablaViewModel model = new TablaViewModel();
            using (productoEntities db= new productoEntities())
            {
                var oTabla = db.crudcreate.Find(Id);
                model.tipo_documento=oTabla.tipo_documento;
                model.Nombre=oTabla.nombre_cliente;
                model.direccion=oTabla.direccion;
                model.telefono = oTabla.telefono;
                model.correo = oTabla.correo;
                model.fecha_creacion=oTabla.fecha_creacion;
                model.Id = oTabla.id;

            }
                return View(model);

        }
        [HttpPost]
        public ActionResult Editar(TablaViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (productoEntities db = new productoEntities())
                    {
                        db.SPUPDATE1(model.Id, model.Nombre, model.tipo_documento, model.direccion, model.telefono, model.correo, model.fecha_creacion);
                        db.SaveChanges();


                    }
                    return Redirect("~/Tabla/");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        [HttpGet]
        public ActionResult Eliminar(int Id)
        {
            
            using (productoEntities db = new productoEntities())
            {
                db.SPDELETE1(Id);
                
                db.SaveChanges();
            }
            return Redirect("~/Tabla/");

        }
    }
}