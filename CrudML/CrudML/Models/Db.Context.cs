﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CrudML.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class productoEntities : DbContext
    {
        public productoEntities()
            : base("name=productoEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<crudcreate> crudcreate { get; set; }
    
        public virtual int SPINSERT1(Nullable<int> identificacion, string nombre_cliente, string tipo_identificación, string direccion, string telefono, string correo_electronico, Nullable<System.DateTime> fecha_de_creacion)
        {
            var identificacionParameter = identificacion.HasValue ?
                new ObjectParameter("identificacion", identificacion) :
                new ObjectParameter("identificacion", typeof(int));
    
            var nombre_clienteParameter = nombre_cliente != null ?
                new ObjectParameter("nombre_cliente", nombre_cliente) :
                new ObjectParameter("nombre_cliente", typeof(string));
    
            var tipo_identificaciónParameter = tipo_identificación != null ?
                new ObjectParameter("Tipo_identificación", tipo_identificación) :
                new ObjectParameter("Tipo_identificación", typeof(string));
    
            var direccionParameter = direccion != null ?
                new ObjectParameter("Direccion", direccion) :
                new ObjectParameter("Direccion", typeof(string));
    
            var telefonoParameter = telefono != null ?
                new ObjectParameter("Telefono", telefono) :
                new ObjectParameter("Telefono", typeof(string));
    
            var correo_electronicoParameter = correo_electronico != null ?
                new ObjectParameter("Correo_electronico", correo_electronico) :
                new ObjectParameter("Correo_electronico", typeof(string));
    
            var fecha_de_creacionParameter = fecha_de_creacion.HasValue ?
                new ObjectParameter("fecha_de_creacion", fecha_de_creacion) :
                new ObjectParameter("fecha_de_creacion", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SPINSERT1", identificacionParameter, nombre_clienteParameter, tipo_identificaciónParameter, direccionParameter, telefonoParameter, correo_electronicoParameter, fecha_de_creacionParameter);
        }
    
        public virtual int SPUPDATE1(Nullable<int> identificacion, string nombre_cliente, string tipo_identificación, string direccion, string telefono, string correo_electronico, Nullable<System.DateTime> fecha_de_creacion)
        {
            var identificacionParameter = identificacion.HasValue ?
                new ObjectParameter("identificacion", identificacion) :
                new ObjectParameter("identificacion", typeof(int));
    
            var nombre_clienteParameter = nombre_cliente != null ?
                new ObjectParameter("nombre_cliente", nombre_cliente) :
                new ObjectParameter("nombre_cliente", typeof(string));
    
            var tipo_identificaciónParameter = tipo_identificación != null ?
                new ObjectParameter("Tipo_identificación", tipo_identificación) :
                new ObjectParameter("Tipo_identificación", typeof(string));
    
            var direccionParameter = direccion != null ?
                new ObjectParameter("Direccion", direccion) :
                new ObjectParameter("Direccion", typeof(string));
    
            var telefonoParameter = telefono != null ?
                new ObjectParameter("Telefono", telefono) :
                new ObjectParameter("Telefono", typeof(string));
    
            var correo_electronicoParameter = correo_electronico != null ?
                new ObjectParameter("Correo_electronico", correo_electronico) :
                new ObjectParameter("Correo_electronico", typeof(string));
    
            var fecha_de_creacionParameter = fecha_de_creacion.HasValue ?
                new ObjectParameter("fecha_de_creacion", fecha_de_creacion) :
                new ObjectParameter("fecha_de_creacion", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SPUPDATE1", identificacionParameter, nombre_clienteParameter, tipo_identificaciónParameter, direccionParameter, telefonoParameter, correo_electronicoParameter, fecha_de_creacionParameter);
        }
    
        public virtual int SPDELETE1(Nullable<int> identificacion)
        {
            var identificacionParameter = identificacion.HasValue ?
                new ObjectParameter("identificacion", identificacion) :
                new ObjectParameter("identificacion", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SPDELETE1", identificacionParameter);
        }
    }
}
