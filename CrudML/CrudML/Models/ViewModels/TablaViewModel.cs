﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CrudML.Models.ViewModels
{
    public class TablaViewModel
    {
        [Required]
        [StringLength(50)]
        [Display(Name = "Tipo de documento:")]
        public string tipo_documento { get; set; }
        [Required]
        [IntegerValidator]
        [Display(Name ="Número de identificación")]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name ="Nombre")]
        public string Nombre { get; set; }
        
        
        [StringLength(50)]
        [Display(Name = "Direccion")]
        public string direccion { get; set; }
        [Required]
        [IntegerValidator]
        [Display(Name = "Telefono")]
        public string telefono { get; set; }
        
        [EmailAddress]
        [Display(Name = "Correo electrónico")]
        public string correo { get; set; }
        
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de creacion")]
        public DateTime? fecha_creacion { get; set; }
    }
}