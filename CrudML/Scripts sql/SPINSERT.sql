USE [producto]
GO
/****** Object:  StoredProcedure [dbo].[SPINSERT1]    Script Date: 15/09/2022 3:11:48 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SPINSERT1]
	-- Add the parameters for the stored procedure here
	@identificacion int,
	@nombre_cliente varchar(20),
	@Tipo_identificación varchar(20),
	@Direccion varchar(20),
	@Telefono varchar(20),
	@Correo_electronico varchar(20),
	@fecha_de_creacion date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO crudcreate(id,nombre_cliente,tipo_documento,direccion,telefono,correo,fecha_creacion)
	VALUES(
	@identificacion,
	@nombre_cliente,
	@Tipo_identificación,
	@Direccion ,
	@Telefono,
	@Correo_electronico,
	@fecha_de_creacion );
	
END
